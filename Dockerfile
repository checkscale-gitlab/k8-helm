FROM debian:buster as helm-builder

ARG REF_NAME
ARG SOPS_VER=3.2.0

ENV HELM_VERSION=$REF_NAME
ENV SOPS_VERSION=$SOPS_VER

WORKDIR /root/

RUN apt-get update \
 && apt-get install -y curl \
 && curl -Ls https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xz \
 && curl -Ls https://github.com/mozilla/sops/releases/download/${SOPS_VERSION}/sops_${SOPS_VERSION}_amd64.deb -o /sops_amd64.deb

FROM mdeterman/k8-kubectl

LABEL maintainer="Mark Determan<mark@determan.io>"

RUN apt-get update \
 && apt-get install -y git curl gsutil \
 && rm -rf /var/lib/apt/lists/*

ARG REF_SLUG

# Metadata
LABEL org.label-schema.vcs-ref=$REF_SLUG \
      org.label-schema.vcs-url="https://github.com/mdeterman/docker/k8s-helm" \
      org.label-schema.docker.dockerfile="/Dockerfile"

COPY --from=helm-builder /root/linux-amd64/helm /usr/local/bin
RUN helm version --client

COPY --from=helm-builder /sops_amd64.deb /sops_amd64.deb

RUN dpkg -i /sops_amd64.deb && \
    rm /sops_amd64.deb

RUN mkdir -p /etc/deploy && \
    helm init --client-only && \
    helm repo add io-determan https://io-determan-charts.storage.googleapis.com

CMD ["helm"]